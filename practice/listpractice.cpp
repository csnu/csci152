#include <iostream>
#include <list>
#include <string>
#include <vector>

void print( std::list< int > lst ) {
        std::cout << "[ ";
        for ( auto it = lst. begin( ); it != lst. end( ); it ++ ) {
                if ( it != lst.begin( ) )
                        std::cout << ", ";
                std::cout << *it;
        }
        std::cout << " ]\n";
}

void shift_to_right( std::list< int > &lst, int n, int m ) {
	
        std::vector< int > vect{ std::begin( lst ), std::end( lst ) };
        for ( size_t i = 0; i != vect. size( ) - n; i ++ ) {

                vect[ i + n ] = vect[ i ];
                vect[ i ] = m;
        }
	lst = std::list< int > { vect. begin( ), vect. end( ) };
}
int main( ) {

        std::string s = "one two three";
// Converts const char* to std::string.
s += ' ';
s += "four five six";
std::cout << s;
for( std::string::const_iterator
p = s. begin( );
p != s. end( );
++ p )
{
std::cout << *s;
}
        std::list<double> ls = { 1, 2, 3, 4, 5 };

        for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {
                double &d = *it;
                d = d + 1;
        }
        double sum = 0;
        for( const double& el : ls )
                sum += el;
        for( auto el: ls )
                std::cout << el << " ";
        std::cout << "\n";
        int* i = new int{20};
        std::cout << *i << std::endl;
        delete i;
        std::cout << *i << std::endl;
        std::list< int > lst = { 1, 4, 3, 7 };
        print( lst );

        shift_to_right( lst, 2, 9 );

        print( lst );
        return 0;
}

