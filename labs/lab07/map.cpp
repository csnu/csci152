/*
 * set.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: benja
 */

#include "map.h"

// From previous task:

bool equal( const std::string& s1, const std::string& s2 ) 
{
	if( s1. size( ) != s2. size( ) )
		return false;
	for( size_t i = 0; i != s1. size( ); i++ ) 
		if( tolower( s1[i] ) != tolower( s2[i] ) )
			return false;
	return true;
}
size_t hash( const std::string& s )
{
	size_t sum = 0;
	size_t h = 1;
	
	for( size_t i = 0; i != s. size( ); i ++ ) {
		h = h * 31 + (size_t) tolower( s[i] );
		sum += h;
	}
	return h;
}
map::listofpairs::const_iterator 
map::find( const listofpairs& lst, const std::string& key )
{
   auto p = lst. begin( );
   while( p != lst. end( ) && !equal( p -> first, key ))
      ++ p;
   return p; 
}

map::listofpairs::iterator
map::find( listofpairs& lst, const std::string& key )
{
   auto p = lst. begin( );
   while( p != lst. end( ) && !equal( p -> first, key ))
      ++ p;
   return p;
}


bool map::contains_key( const std::string& key ) const
{
	const map::listofpairs &ls = getbucket( key );
	return find( ls, key ) != ls. end( );
}

bool map::insert( const std::string& key, unsigned int val )
{
	check_rehash( );
	listofpairs &ls = getbucket( key );
	
	if( !contains_key( key ) )
	{
		ls. push_back( { key, val } );		
		map_size++;
		return true;
	}
	return false;
}

unsigned int& map::operator[] ( const std::string& key ) 
{
	listofpairs &ls = getbucket( key );

	if( contains_key( key ) )
	{
		listofpairs::iterator p = find(ls , key );
		return p->second;
	}	
	else {
		check_rehash( );
		ls. push_back( { key, 0 } );
		map_size++;
		return (find(ls, key))->second;
	}
}
unsigned int& map::at( const std::string& key )
{
	listofpairs &ls = getbucket( key );
	if( contains_key( key ) )
	{
		listofpairs::iterator p = find(ls , key );
		return p->second;
	}	
	else
		throw std::out_of_range( "at( ): string not found" );
}
unsigned int map::at( const std::string& key ) const {
	const listofpairs &ls = getbucket( key );
	
	if( contains_key( key ) )
	{
		listofpairs::const_iterator p = find( ls, key );
		return p->second;
	}	
	else
		throw std::out_of_range( "at( ): string not found" );
}

void map::rehash( size_t newbucketsize ) 
{
	if( newbucketsize < 4 )
		newbucketsize = 4;

	std::vector< listofpairs > newbuckets{ newbucketsize };
	std::vector< listofpairs > oldbuckets = buckets;
	
	buckets = newbuckets;
	
	listofpairs ls;
	map_size = 0;
	for( size_t i = 0; i != oldbuckets. size( ); i ++ ) {
		ls = oldbuckets[i];

		for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {
			getbucket(it->first). push_back( *it );
			map_size++;
		}
	}
}
	
void map::check_rehash( ) 
{
	if( loadfactor( ) > max_load_factor )
		rehash( buckets. size( ) * 2 );
}

bool map::remove( const std::string& key ) 
{
	listofpairs &ls = getbucket( key );
	listofpairs::iterator it = find( ls, key );

	if( it != ls. end( ) )
	{	
		ls. erase( it );		
		map_size--;
		return true;
	}
	return false;
}


double map::standarddev( ) const  
{
   double sum = 0.0;
   double lf = loadfactor();

   for ( const auto& l : buckets )
   {
      double dif = l.size() - lf;
      sum += dif*dif;
   }

   return sqrt( sum / buckets. size( ));
}

void map::clear( ) 
{
   for( auto& l : buckets )
      l. clear( ); 

   map_size = 0; 
}


std::ostream& map::print( std::ostream& out ) const
{
	size_t index;
	listofpairs ls;

	for( size_t i = 0; i != buckets. size( ); i ++ ) {

		out << "bucket[" << i << "]:\t{ ";
		ls = buckets[i];
		for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {

			if( it != ls. begin( ) )
				out << ", ";

			out << "{" << it->first << ": " << it->second << "}";			
		}
		out << " }\n";
	}
	return out;	
}

std::ostream& map::printstatistics( std::ostream& out ) const
{
   out << "set size =            " << size( ) << "\n";
   out << "load factor =         " << loadfactor( ) << " ( max = " << 
                                      max_load_factor << " )\n";
   out << "standard deviation =  " << standarddev( ) << "\n";
   return out;
}


