public class StopWatch {
	
	private long start;
	private long end;

	public StopWatch( ) { 
		start = System.currentTimeMillis();	
	} 
	
	public void clear() {
		start = System.currentTimeMillis();
	}
	public long time() {
		return System.currentTimeMillis() - start;
	}
}
