
#ifndef STRING_INCLUDED 
#define STRING_INCLUDED 1

#include <iostream> 
#include <cstring>

class string
{
   size_t len;
   char *p; 

public: 
   string( )
      : len{0},
        p{ new char[0] }
   {
      std::cout << "Default\n";
   }

   string( const string& s ) 
      : len{ s. len },
        p{ new char[ len ] }
   {
      for( size_t i = 0; i != len; ++ i )
         p[i] = s.p[i];
   }

   string( const char* c )
      : len{ strlen(c) },
        p{ new char[len] }
   {
      std::cout << "constructing from const char*: " << c << "\n";
      for( size_t i = 0; i != len; ++ i )
         p[i] = c[i];
   }

   ~string( )
   {
      delete[] p;  
   }
   size_t size( ) { return len; }

   
   const string& operator = ( const string& s ) {
      if( len != s. len ) {
         delete[] p;
         len = s. len;
         p = new char[ len ];
      }
      for( size_t i = 0; i != len; ++ i )
      p[i] = s.p[i];
      return s;
   }
   friend 
   std::ostream& operator << ( std::ostream& out, const string& s );
      // Give operator << access to the private fields. This makes
      // sense, because it is almost an 'essential operator'. 
};

std::ostream& operator << ( std::ostream& out, const string& s );

#endif

