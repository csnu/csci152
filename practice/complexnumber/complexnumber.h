#include <iostream>

class complexnumber { 
	double real;
	double imagine;

public:
	complexnumber( )  
	   : real{ 0 }, 
	     imagine{ 0 }
	{ }
	
	complexnumber( const complexnumber& c )  
           : real{ c. real }, 
	     imagine{ c. imagine } {}		

};
