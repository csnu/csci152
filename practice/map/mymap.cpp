#include <iostream>
#include <map>
#include <string>

struct cmp {
	bool operator( ) ( const std::string& s1, const std::string& s2) const {
		return s1. size( ) < s2. size( );
	}
};
int main( ) {

	std::map< std::string, int, cmp > mp;
	mp["abc"] = 1;
	mp["xx"] = 4;
	mp["AbC"]= 3;
	mp["hi"] = 4;

	std::cout << mp["CSCI"] << " " << mp["152"] << "\n";

	for( auto& i: {"have", "a", "nice", "day"} ) {
		std::string s = i;
		auto p = mp. insert( { s, s. size( ) });
		if( ! p. second )
			std::cout << "inserted" ;
	}

	for( const auto& p: mp )
		std::cout << p. first << "==>" << p. second << "\n";
	return 0;
}

