#include <iostream>
#include <string>
#include <vector>

std::string getmonthname( unsigned int month ) {
	std::vector< std::string > lst = { "January", "February", "March", "April", 
		"May", "June", "July", "August", "September", "October", 
		"November", "December" };

	return lst. at( month - 1 );
}

bool isleapyear( unsigned int year ) {
	return ( year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0;
}

int daysinmonth( unsigned int month, unsigned int year ) { 
	
	if ( month == 1 || month == 3 || month == 7 || month == 8 || 
			month == 10 || month == 12 ) 
		return 31;
	else if ( month == 4 || month == 6 || month == 9 || month == 11 ) 
		return 30;
	else {
		if ( isleapyear( year ) ) 
			return 29;
		return 28;
	}
	return -1;	
}

unsigned int gettotaldays( unsigned int day, unsigned int month, 
				unsigned int year ) {
	
	int days = 1;
	
	for ( size_t i = 1; i < year; i ++ ) {
		for ( size_t j = 1; j <= 12; j++ ) 
			days += daysinmonth( j, i );
	}
	
        for ( size_t j = 1; j < month; j++ )
                days += daysinmonth( j, year );

	return days % 7;
}
void printcalendar( unsigned int day, unsigned int month, unsigned int year ) {
	
	std::string monthname = getmonthname( month );

	for ( size_t i = 0; i < 14 - monthname. size( ) / 2; i++ ) 
		std::cout << " ";

	std::cout << monthname << "\n";
	std::cout << "Sun " << "Mon " << "Tue " << "Wed " 
				<< "Thu " << "Fri " << "Sat \n";
	unsigned int totaldays;
	unsigned int days;

	totaldays = gettotaldays( day, month, year );

	days = daysinmonth( month, year ); 

	for ( int i = 0; i < totaldays; i++ ) 
		std::cout << "    ";
	for ( int i = 1; i <= days; i ++ ) { 
		if ( i < 10 ) 
			std::cout << i << "   ";
		else
			std::cout << i << "  ";

		totaldays++;
		if ( totaldays % 7 == 0 ) 
			std::cout << "\n";
	}
	std::cout << "\n";
}
int main( ) {

	std::cout << "Welcome calendar that much better Maya's\n";
	
	unsigned int day;
	unsigned int month;
	unsigned int year;
	
	std::cout << "Enter a day: "; 
 	std::cin >>  day;
	std::cout << "Enter a month (e.g. 1 for January): "; 
	std::cin >> month;
	std::cout << "Enter a year: "; 
	std::cin >>  year;
	
	printcalendar( day, month, year );

	return 0;
}
