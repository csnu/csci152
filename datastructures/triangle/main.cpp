#include <iostream>
#include "triangle.h"

int main( ) {
	triangle tr;

	tr. setA( 3 );
	tr. setB( 4 );
	tr. setC( 5 );
	
	triangle tr2;
	tr2. setA( 3 );
	tr2. setB( 4 );
	tr2. setC( 6 );
	std::cout << tr. equals( tr2 ) << std::endl;
	std::cout << tr << std::endl;
	return 0;
}
