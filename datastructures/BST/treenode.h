#ifndef TREE_INCLUDED
#define TREE_INCLUDED    1 

#include <iostream>

struct treenode {
	double val;
	treenode* left;
	treenode* right;
	   
	treenode( double d ) 
	: val{ d },
	  left{ nullptr },
	  right{ nullptr } { }

public: 
	bool contains( treenode* root, double d ) {
		if( root == nullptr )
			return false;
		else if( root->val = d )
			return true;
		else if( d < root->val ) 
			return contains( root->left, d);
		else
			return contains( root->right, d);
		
	}

	treenode* insert( treenode* root, double d ) {
		//if( !contains( root, d ) ) {
			if( root == nullptr ) {
				root = new treenode(d);
			} else if( d < root->val)
				root->left = insert(root->left, d);
			else
				root->right = insert( root->right, d );
			return root;	
		//}
		return nullptr;
	}

	void inorder( std::ostream& out, treenode* root ) {
		if( root == nullptr ) 
			return;
		
		inorder(out, root->left);
		out << root->val << " ";
		inorder(out, root->right);

	}

	void preorder( std::ostream& out, treenode* root ) {
		if( root == nullptr ) 
			return;

		out << root->val << " ";
		preorder(out, root->left);
		preorder(out, root->right);
	}
	void postorder( std::ostream& out, treenode* root ) {
		if( root == nullptr ) 
			return;

		postorder(out, root->left);
		postorder(out, root->right);
		out << root->val << " ";
	}

	void reverseorder( std::ostream& out, treenode* root ) {
		if( root == nullptr ) 
			return;

		reverseorder(out, root->right);
		out << root->val << " ";
		reverseorder(out, root->left);
	}
};


#endif