#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "stopwatch.h"

void shuffle( std::vector< int > &x ) {
	
	for ( size_t i = 0; i < x. size( ); i++ ) {
		
		int random = rand ( ) % 9;
		std::swap( x[i], x[random]);
	}
}

void print( std::vector< int > x ) {
	
	for ( int i : x) {
		std::cout << i << " ";
	}
	std::cout << "\n";
}

bool isEqual( const std::vector< int > x, 
			const std::vector < int > y ) {

	if ( x. size( ) != y. size ( ) ) 
		return false;
	for ( size_t i = 0; i < x. size( ); i ++ ) {
		if ( x[i] != y[i] ) 
			return false;
	}
	return true;
}
void fillarray( std::vector< int > &x , int n ) {
	for ( size_t i = 0; i != n; ++ i ) { 
		x. push_back( i % 9 ); 
	}
}
int main( )
{
	srand( time( NULL ) );
	std::vector< int > x;
	
	stopwatch st;
	fillarray( x, 15 );
	
	std::vector< int > y = x;

	do {
		print ( x );
		print ( y );
		shuffle ( y );
		if ( st. time( ) > 30 ) {
			std::cout << "Execution time exceeds the limit\n";
			break;
		} 
			      
	} while ( !isEqual( x, y ) );
	
	print( x );
	print( y );
	
	std::cout << "overall time spent for execution of 15 index array" <<  
  			" shuffling is " << st. time( ) << " seconds\n";
	return 0;
}
