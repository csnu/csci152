#include <iostream>
#include <string>
#include <unordered_set>
#include <vector>


void print( std::vector< std::string > vect ) {
	for( size_t i = 0; i != vect. size( ); i ++ ) 
		std::cout << vect. at(i) << " ";
	std::cout << std::endl;
}

void print( std::unordered_set< std::string > set ) {
	for( auto it = set. begin( ); it != set. end( ); it ++ )
		std::cout << *it << " ";
	std::cout << "\n";
}

std::unordered_set< std::string > getunion( 
			std::unordered_set< std::string > s1, 
				std::unordered_set< std::string > s2 ) {

	std::unordered_set< std::string > target;
	for( auto it = s2. begin( ); it != s2. end( ); it ++ ) 
		target. insert( *it );
	for( auto it = s1. begin( ); it != s1. end( ); it ++ ) 
		target. insert( *it );

	
	return target;
}

int main( ) {

	std::unordered_set< std::string > set1 = { "a", "b", "c", "d", "e", "f" };
	std::unordered_set< std::string > set2 = { "b", "c", "R" };

	print( getunion( set1, set2) );	
	return 0;
}
