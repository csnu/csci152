#include <iostream>
#include <algorithm>
#include <vector>
#include "stopwatch.h"

std::vector< int > randomvector( size_t k )
{
   std::vector< int > res;
   for( size_t i = 0; i != k; ++ i )
      res. push_back( rand( ));
   return res;
}

void bubble_sort( std::vector< int > & vect )
{
   if( vect. size( ) < 2 ) return;
      // Every shorter vector is sorted.

   bool sorted = false;
   while( !sorted )
   {
      sorted = true;
      for( size_t i = 0; i + 1 != vect. size( ); ++ i )
      {
         if( vect[i] > vect[i+1] )
         {
            std::swap( vect[i], vect[i+1] );
            sorted = false;
         }
      }
   }

}

int main( ) {

	size_t n = 4000;
	stopwatch tt;
	std::vector< int > vect = randomvector(n);
	bubble_sort( vect );
	std::cout << tt. time( ) << "\n";

	return 0;
}
