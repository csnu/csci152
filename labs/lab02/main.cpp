
#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "queue.h"

void teststqueue( )
{
   queue q1 = { 1, 2, 3, 4, 5 };
   queue q2 = q1; // Copy constructor.
   
   for( unsigned int j = 0; j < 30; ++ j )
   {
      q1. push( j );
      std::cout << q1. peek( ) << "\n";
      q1. pop( );
   }

   q1 = q2; // Assignment.
   q1 = q1; // Self assignment.
   std::cout << "q1 after self assignment = " << q1 << "\n";
   q1 = { 100,101,102,103 };
   
   std::cout << "q1 = " << q1 << "\n";
   std::cout << "q2 = " << q2 << "\n";

   queue q3 = { 1, 2, 3, 4, 5, 6 };
   std::cout << q3 << "\n";
   
   for( unsigned int i = 0; i < 10000; ++ i )
   {
      q3. push(i); q3. checkinvariant( );
      double d = q3. peek( ); q3. checkinvariant( );
      q3. pop( ); q3. checkinvariant( );
      q3. push(d); q3. checkinvariant( );
   }

   std::cout << "q3 size = " << q3.size() << "\n";
  
   q3.clear();
   
   std::cout << "q3 (after we clean it): " << q3 << "\n"; 
}


int main( int argc, char* argv [ ] )
{
   queue queue1;

   queue1. push( 2 );
   queue1. push( 3 );
   queue1. push( 4 );

   queue& queue2 = queue1; 

   queue2. pop();
   queue1. push( 5 );
   std::cout << queue1 << std::endl;
   std::cout << queue2 << std::endl;

   std::cout << "      teststqueue\n";
   std::cout << "----------------------\n";

   teststqueue();

   std::cout << "\n     main() function\n";
   std::cout << "------------------------\n";
   queue q;

   q. push(1); q. push(2); q. push(3);
   q. push(4); q. push(5); q. push(6);
   q. push(7); q. push(8);

   std::cout << q << "\n";
  
   while( q. size( ) > 3 )
   {
      double d = q. peek( );
      std::cout << d << "\n";
      q. pop( );
   }

   queue q2 = q;
   std::cout << q << "\n";
   std::cout << q2 << "\n";

   while( q. size( ) > 3 )
   {
      double d = q. peek( );
      std::cout << d << "\n";
      q. pop( );
   }
   q2 = q;
   std::cout << q << "\n";
   std::cout << q2 << "\n";

   queue q3 = { 1,2,3,4,5,6 };
   std::cout << q3 << "\n";

   for( unsigned int i = 0; i < 10000; ++ i )
   {
      q3. push(i); q3. checkinvariant( );
      double d = q3. peek( ); q3. checkinvariant( );
      q3. pop( ); q3. checkinvariant( );
      q3. push(d); q3. checkinvariant( );
   }

      
   return 0;
}

