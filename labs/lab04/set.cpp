
// Lab Exercise 4.

#include "set.h"
#include <string>
#include <vector>

bool set::equal( const std::string& s1, const std::string& s2 )
{
        if( s1. size( ) != s2. size( ) )
                return false;

        for( size_t i = 0; i != s1. size( ); i ++ )
                if( tolower( s1[i] ) != tolower( s2[i] ) )
                        return false;
        return true;
}

bool set::contains( const std::string& el ) const {

	for( size_t i = 0; i != data. size( ); i ++ ) 
		if( equal( data. at(i), el ) )
			return true;
	return false;		
}

bool set::insert( const std::string& el ) {
	
	if( !contains( el ) ) {
		data. push_back( el );
		return true;
	}
	
	return false; 
}	

size_t set::insert( const set& s )
{
	size_t count = 0;
	for ( auto p = s. begin( ); p != s. end( ); p ++ ) 
		if( insert( *p ) )
			count++;
	
	return count; 
}

bool set::remove( const std::string& el ) {

	for ( size_t i = 0; i != data. size( ); i ++ ) {
		if( equal( data[i], el ) ) {

			std::swap( data[i], data. back( ) ); 
			data. pop_back( );

			return true;
		}
	}
	return false;
}
size_t set::remove( const set& s )
{
	size_t count = 0;

	for ( auto q = s. begin( ); q != s. end( ); q ++ ) {
		if( remove( *q ) ) 
			count++;
	}

	return count;
}

std::ostream& operator << ( std::ostream& out, const set& s )
{
	out << "[ ";

	for ( auto p = s. begin( ); p != s. end( ); p ++ ) {
		if( p != s. begin( ) ) 
			out << ", ";
		out << *p;
	}

	out << " ]";

	return out;
}

bool subset( const set& s1, const set& s2 ) {

	bool issubset = false;

	if ( s1. size( ) < s2. size( ) )
		return false;

	std::vector< std::string > vect1; //vectors are preferred as they have indeces
	std::vector< std::string > vect2;
	
	for( auto p = s1. begin( ); p != s1. end( ); p ++ ) 
		vect1. push_back( *p ); //now vect1 contains contents of s1
	
	for( auto p = s2. begin( ); p != s2. end( ); p ++ )
		vect2. push_back( *p ); //vect2 contains contents of s2 as well

    size_t length1 = vect1. size( );
    size_t length2 = vect2. size( );
	int k;

	//this loop iterates through vect1 and checks whether vect1 
	//has the same sequence of elements that in vect2 
    for( size_t i = 0; i < length1 - length2 + 1; i ++ ) {
		k = 0;
        for( size_t j = i; j < i + length2; j ++, k++ ) {
			if( set::equal( vect1[j], vect2[k] ) )
				issubset = true;
			else {
				issubset = false;
				break;
			}
        }
        if( issubset ) 
            return true;
    }
    
    return false;
}	

set::set( std::initializer_list< std::string > init ) {

	for( std::string str: init ) {
		if( !contains( str ) )
			insert( str );
	}
}

