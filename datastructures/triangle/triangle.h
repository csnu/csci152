#ifndef TRIANGLE
#define TRIANGLE 1

#include <iostream>
#include <cmath>
class triangle {
	double a, b, c;

public:
	triangle( )
	:	a{ 1 }, 
		b{ 1 },
		c{ 1 }  {
			std::cout << "A triangle created with sides 1, 1, and 1\n";
	}

	triangle( const triangle& tr )
	:  a{ tr. a },
	   b{ tr. b }, 
	   c{ tr. c }  {
	   	std::cout << "A triangle created with sides " << a << ", " << b << ", and " << c << std::endl;
	}
	void setA( double a1 ) {a = a1;}
	void setB( double b1 ) {b = b1;}
	void setC( double c1 ) {c = c1;}

	double area( ) {
		double p = (a + b + c) / 2;
		double area = std::sqrt( p * (p-a) * (p-b) * (p-c));
		return area;
	}
	bool equals( const triangle& tr ) {
		return a == tr. a && b == tr. b && c == tr. c;
	}
	std::ostream& print( std::ostream& out ) {
	   	out << "The triangle now has sides " << a << ", " << b << ", and " << c 
	   				<< "    | Area = " << area( );
		return out;
	}
	friend
	std::ostream& operator << (std::ostream& out, triangle& tr) {
		return tr. print( out );
	}
};
#endif