#include <iostream>
#include "stack.h"

stack merge_ordered( stack s1, stack s2 ) {
	stack temp;
	stack ordered;

	while( s1. size( ) != 0 || s2. size( ) != 0 ) {
		double n1 = s1. peek( );
		double n2 = s2. peek( );

		if( n1 > n2 ) { temp. push(n1); temp. push(n2);}
		else {temp. push(n2); temp. push(n1);}
		s1. pop( );
		s2. pop();
	}

	while( !temp. empty( )) {
		//std::cout << temp. peek( ) << " ";
		ordered. push( temp. peek( ));
		temp. pop();
	}
	//std::cout << "\n";
	return ordered;
}
int main() {

	stack s1 = { -8, 4, 5, 10, 14 };
	stack s2 = { -2, -2, 8, 9, 12 };

	std::cout << merge_ordered( s1, s2 ) << std::endl;
	
	return 0;
}	
