#include <random>
#include <sstream>

#include "set.h"
#include "timetable.h"
#include "timer.h"

// Append a number to string.

std::string addnumber( std::string s, long unsigned int i ) 
{
   std::ostringstream ind;
   ind << i;
   s += ind. str( );
   return s;
}
 

int main( int argc, char* argv[ ] )
{
   set set1 = { "a", "b", "c", "d", "e", "f", "F", "A" };

   set set2 = { "a", "B", "C", "D", "e", "F" };

   std::cout << set1 << std::endl << set2 << std::endl;

   set1. insert( "F" );
   set1. insert( set2 );
   std::cout << set1 << std::endl << set2 << std::endl;

   if( subset( set1, set2 ) )
	std::cout << "set2 is subset of set1" << std::endl;
   else
	std::cout << "not subset" << std::endl;

//   set2. clear( );
   std::cout << "( after cleaning ) set2. size( ) = " << set2. size( ) << std::endl; 

   set1. remove( "A" );
   set1. remove( "F" );


   std::cout << "set1 (after removing a and f ) " << set1 << std::endl;
	
   size_t i = set1. remove( set2 );
   std::cout << "set1 (after removing set2 ) " << set1 << "which has only " << i << std::endl;

   set kaz; 

   std::cout << set::equal( "aStana", "AsTaNa" ) << "\n";
      // true  
   std::cout << set::equal( "astana", "Almaty" ) << "\n";
      // false. 
   std::cout << set::equal( "astana", "astana" ) << "\n"; 
      // true. 

   kaz. insert( "aqtobe" ); 
   std::cout << kaz. contains( "Aqtobe" ) << "\n";
   std::cout << kaz. contains( "Aqtau" ) << "\n";

   kaz. insert( "SZYMKENT" );
   std::cout << kaz. contains( "szymkent" ) << "\n";
   set set3;
   set3 = kaz;
   std::cout << "set3: " << set3 << std::endl;
   std::cout << kaz << "\n"; 
   kaz. remove( "Szymkent" );
   std::cout << kaz << "\n";
   kaz. remove( "Aqtobe" );
   std::cout << kaz << "\n";
#if 1
   timetable tab( std::string( "set" ));
   for( size_t s = 1000; s < 20000; s = 2 * s ) 
   {
      set someset1;

      timer tt; 
      size_t nr = 0;
      for( size_t k = 0; k != s; ++ k )
      {
         nr += someset1. insert( addnumber( "aa", rand( )));
         nr += someset1. insert( addnumber( "bb", rand( )));
      }

      auto someset2 = someset1;

      if( nr != someset1. size( )) throw std::runtime_error( "counting went wrong" );

      for( const auto& el : someset2 ) 
      {
         nr -= someset1. remove( el );  
      }

      if( nr != 0 || someset1. size( ) != 0 )
         throw std::runtime_error( "counting went wrong" ); 

      tab. insert( s, tt. time( )); 
   } 
  
   std::cout << tab << "\n"; 
   std::cout << "totaltime " << tab. totaltime( ) << "\n";
#endif
}


