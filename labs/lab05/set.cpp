
#include "set.h"

void print( std::ostream& out, const treenode* n, size_t indent )
{  
   for( size_t i = 0; i != indent; ++ i )
      out << "|  "; 
   if(n)
   {
      out << ( n -> val ) << "\n";
      print( out, n -> left, indent + 1 );
      print( out, n -> right, indent + 1 ); 
   }
   else
      out << "#\n"; 
}


void checksorted( 
          treenode* n, 
          const std::string* lowerbound, 
          const std::string* upperbound )
{
   while(n) 
   {
      if( lowerbound && !before( *lowerbound, n -> val ))
      {
         std::cout << "value " << ( n -> val );
         std::cout << " is not above lower bound " << *lowerbound << "\n";
         std::abort( );
      }

      if( upperbound && !before( n -> val, *upperbound ))
      {
         std::cout << "value " << ( n -> val );
         std::cout << "is not below upperbound " << *upperbound << "\n";
         std::abort( );
      }

      checksorted( n -> left, lowerbound, &( n -> val ));
      lowerbound = & ( n -> val );
      n = n -> right;
   }
} 


// Used by copy constructor and assignment:

void writecopy( treenode** to, treenode* from )
{
   while( from )
   {
      *to = new treenode{ from -> val };

      writecopy( & (*to) -> left, from -> left );
      to = & (*to) -> right;
      from = from -> right;
   }
}

// Used by destructor:

void deallocate( treenode* n )
{
   while(n)
   {
      deallocate( n -> left );
      treenode* bye = n;
      n = n -> right;
      delete bye;
   }
}



size_t log_base2( size_t s ) { 

	size_t count = 0;

  while( s > 1 ) {
    count++;
    s /= 2;
  }
  return count;
}

// From previous task:

bool equal( const std::string& s1, const std::string& s2 ) {
      
    if( s1. size( ) != s2. size( ) )
        return false;

    for( size_t i = 0; i != s1. size( ); i ++ )
        if( tolower( s1[i] ) != tolower( s2[i] ) )
                return false;
    return true;
}

// Must be written for task 5(part 2) :

bool before( const std::string& s1, const std::string& s2 ) {
	
	for( size_t i = 0; i != s1. size( ) && i != s2. size( ); i ++ ) {
		if( tolower( s1[i] ) > tolower( s2[i] ) )
			return false;
		else if ( tolower( s1[i] ) < tolower( s2[i] ) )
			return true;
		else
			continue;
	}
	if ( s1. size( ) < s2. size( ) )
		return true;

	return false;
}


const treenode* find( const treenode* n, const std::string& el ) {
	
	if( n ) {
		if( n->val == el )
			return n;
		else if ( before( el, n->val ) ) 
			return find( n->left, el );
		else
			return find( n->right, el );
	}
  return nullptr;
}
   // Write this one first.

treenode** find( treenode** n, const std::string& el ){
	
	const treenode* t = find(*n, el);

  if ( before( el, t->val) )
    return &((*n)->left);
  else
    return &((*n)->left);
}
   // Modify the other find into this one, also have a look at
   // stackscan4.pdf


// Insert n at the right most position in into: 

void rightinsert( treenode** into, treenode* n );


size_t size( const treenode* n ) {
	
  if( n == nullptr )
    return 0;
  else {
    return 1 + size(n->left) + size(n->right);
  }
}	

size_t height( const treenode* n ) {

  const treenode* root = n;
  size_t count = 0;
  size_t temp;

  if( !n  || ( n->left == nullptr && n->right == nullptr ) )
    return count;
  else {
      count++;
      while(n) {
          n=n->left;
          count++;
          continue;
      }
      temp = count;
      count = 0;
      n = root;

      while(n) {
          n=n->right;
          count++;
          continue;
      }
      return (temp > count) ? temp : count;
  }
}

 
bool set::insert( const std::string& el ) {
	
  if( !contains( el ) ) {
    while( tr ) {
      if( tr == nullptr ) {
        tr = new treenode( el );
        return true;
      }
      //else {
          if( before( el, tr->val) ) {
            tr->left = tr;
            continue;
          }
          else {
            tr->right = tr;
            continue;
          } 
      //}
    }
  }
  return false;
}

bool set::contains( const std::string& el ) const 
{
   return find( tr, el ) != nullptr; 
}


bool remove( const std::string& el );


void set::checksorted( ) const
{
   ::checksorted( tr, nullptr, nullptr );
      // :: are needed because the other checksorted is not in the class. 
}


std::ostream& set::print( size_t indent, std::ostream& out ) const
{
   ::print( out, tr, indent );
   return out;
}


