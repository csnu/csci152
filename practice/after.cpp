#include <iostream>
#include <string>
#include <set>

bool after( std::string s1, std::string s2 ) {
	for( size_t i = 0; i != s1. size( ) && i != s2. size( ); i ++ ) {
		if( tolower( s1[i] ) > tolower( s2[i] ) )
			return true;
		else if( tolower( s1[i] ) < tolower( s2[i] ) )
			return false;
		else
			continue;
	}
	if( s1. size( ) > s2. size( ) ) return true;
	return false;
}

int main( ) {
	


	#if 0
	std::cout << after( "Wendy", "Karl" ) << std::endl;
	std::cout << after( "Kart", "Wendy" ) << std::endl;
	std::cout << after( "Astana", "ast" ) << std::endl;
	std::cout << after( "Ast", "astana" ) << std::endl;
	
	std::set< int > set1 = { 1, 2, 3, 4 };
	std::set< int > set2 = { 3, 4, 6, 8 };
	std::set< int > target;
	for( auto it = set1. begin( ); it != set1. end( ); it ++ ) { 
		if( !set1. find( *it ) )
			target. insert( *it );
	#endif

	return 0;
}
