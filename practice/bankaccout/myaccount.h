#include <iostream>
#include <string>

class myaccount 
{
	int id;
	std::string name;
	double amount;
public:
	myaccount ( ) 
	: id{ 0 }, 
	  name { "" }, 
	  amount{ 0 } 
	{}

	myaccount ( const myaccount& a ) 
	: id{ a.id }, 
	  name{ a. name }, 
	  amount{ a. amount } 
	{
		std::cout << "id: " << id << "\n";
		std::cout << "name: " << name << "\n";
		std::cout << "amount: " << amount << "\n";
	}
	void setid( int new_id ) {
		id = new_id;
	}
	void setname( std::string new_name ) {
		name = new_name;
	}
	void setamount( double new_amount ) {
		amount = new_amount;
	}
	void printfields( ) {
		std::cout << "id: " << id << "\n";
		std::cout << "name: " << name << "\n";
		std::cout << "amount: $" << amount << "\n";
	}
	void transfer( myaccount& account ) {
		if ( account. amount == 0 ) 
			std::cout << "this account has no money" << "\n";
		amount += account. amount;
	}
	const int getid( ) const { return id; }

	const double getamount( ) const { return amount; }
	
	const std::string getname( ) const { return name; }

	const myaccount& operator = ( const myaccount&  account ) { 
	
		id = account. getid( );
		name = account. getname( ); 
		amount = account.getamount( );
		    
		return account;
	}
	
};

