#include <iostream>
#include <string>
#include <map>

struct cmp {
	bool operator( ) ( const std::string &s1, const std::string &s2 ) const {
		return s1. size( ) < s2. size( );
	}
};
int main( ) {
	
	std::map< std::string, unsigned int, cmp > mp;

	mp["a"] = 8;
	mp["abc"] = 3;
	mp["xx"] = 5;
	mp["www"] = 7;

	std::cout << mp["CSCI"] << mp["152"] << std::endl;
	
	for( const auto& s: {"have", "a", "nice", "day", "homie"}) {
		std::string str = s;
		mp[str] = str.size();
		auto p = mp. insert( { str, str. size( ) });
		if( !p. second )
			std::cout << str << " is not inserted\n";
	}
	for( const auto &it: mp )
		std::cout << it. first << "/" << it. second << std::endl;

	return 0;
}

