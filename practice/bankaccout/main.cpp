#include <iostream>
#include "myaccount.h"

int main( ) {
	
	myaccount account; 
	account.setid( 1234 );
	account.setname( "Max");
	account.setamount( 10000 ); 
	
	std::cout << "account data:" << "\n";
	account.printfields( );

	myaccount account1;
	account1.setid( 2341 );
	account1.setname( "Mades" ); 
	account1.setamount( 5000 );

	account.transfer(account1);

	std::cout << "account data (after transfer): " << "\n";
 	std::cout << "Total money in this account $" << 
				account. getamount() << "\n";
	
	account1 = account;
	std::cout << "account1 data:" << "\n";
	account1.printfields( );


	return 0;
}
