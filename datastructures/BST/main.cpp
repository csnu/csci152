#include <iostream>
#include "treenode.h"
#include <cmath>
#include <ctime>
#include <cstdlib>

int main( ) {
	
//	srand( time(1) );
	treenode* tr = new treenode(8);

	for( size_t i = 0; i < 10; i++ )
		tr = tr->insert( tr, i );

	tr->inorder( std::cout << "Inorder: ", tr ); std::cout << "\n";

	tr->preorder( std::cout << "Preorder: ", tr ); std::cout << "\n";

	tr->postorder( std::cout << "Postorder: ", tr ); std::cout << "\n";

	tr->reverseorder( std::cout << "Reverseorder: ", tr ); std::cout << "\n";
/*
	std::cout << (tr->contains( tr, 6 ) ? "contains" : 
			"not contains" ) << std::endl;
	std::cout << (tr->contains( tr, 3 ) ? "contains" : 
			"not contains" ) << std::endl;
	std::cout << (tr->contains( tr, 4 ) ? "contains" : 
			"not contains" ) << std::endl;
*/
	return 0;
}
