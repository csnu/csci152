
#ifndef STACK_INCLUDED 
#define STACK_INCLUDED  1

#include <iostream>
#include <initializer_list>

class stack 
{
   size_t current_size;
   size_t current_capacity; 

   double* data; 
      // INVARIANТ: has been allocated with size current_capacity.
      // 0 <= current_size <= current_capacity. 

   void ensure_capacity( size_t c );
      // Ensure that stack has capacity of at least c.
      // This function is given, so you don't have to write it. 
   
public: 
   stack( );            
      // Constructs empty stack. 

   stack( const stack& s );


   ~stack( )  { delete[] data; }

   const stack& operator = ( const stack& s ); //see stack.cpp

   stack( std::initializer_list<double> init );
   
   // So that you can write s = { 1,2,3 };
   // You can use init. size( ) to see the size, and
   // for( double d : init ) to go through all 
   // elements in the initializer_list. 

   void push( double d ); //see stack.cpp

   void pop( ) { --current_size; }
   
   void clear( ) { reset(0); }
 
   void reset( size_t s ) { while( current_capacity > s )  pop( ); } 

   double peek( ) const { return data[ current_size - 1 ];	}

   size_t size( ) const { return current_size; }

   bool empty( ) const { return current_size == 0; }   
  
   friend std::ostream& operator << ( std::ostream& , const stack& ); 
}; 

std::ostream& operator << ( std::ostream& , const stack& );

#endif
