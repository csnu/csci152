
#ifndef STACK_INCLUDED 
#define STACK_INCLUDED  1

#include <iostream>
#include <initializer_list>

class stack 
{
   size_t current_size;
   size_t current_capacity; 

   double* data; 
      // INVARIANТ: has been allocated with size current_capacity.
      // 0 <= current_size <= current_capacity. 

void ensure_capacity( size_t c ) 
{
    if( current_capacity < c )
    {
      // New capacity will be the greater of c and
      // 2 * current_capacity. 

      if( c < 2 * current_capacity )
         c = 2 * current_capacity;

      double* newdata = new double[ c ];
      for( size_t i = 0; i < current_size; ++ i )
         newdata[i] = data[i];

      current_capacity = c;
      delete[] data;
      data = newdata;
    }
}

      // Ensure that stack has capacity of at least c.
      // This function is given, so you don't have to write it. 
   
public: 
   stack( ) 
	: current_size{ 0 }, 
	  current_capacity{ 17 }, 
	  data{ new double[current_capacity] }
   {}  
      // Constructs empty stack. 

   stack( const stack& s );


   ~stack( )  { delete[] data; }

   const stack& operator = ( const stack& s ); //see stack.cpp

   stack( std::initializer_list<double> init );
   
   // So that you can write s = { 1,2,3 };
   // You can use init. size( ) to see the size, and
   // for( double d : init ) to go through all 
   // elements in the initializer_list. 

   void push( double d ) {
	ensure_capacity( current_size + 1 );
	data[ current_size ] = d; 
	current_size++;
   }

   void pop( ) { --current_size; }
   
   void clear( ) { reset(0); }
 
   void reset( size_t s ) { current_size = s; } 

   double peek( ) const { return data[ current_size - 1 ];	}

   size_t size( ) const { return current_size; }

   bool empty( ) const { return current_size == 0; }   
  
   friend std::ostream& operator << ( std::ostream& , const stack& ); 
}; 

std::ostream& operator << ( std::ostream& , const stack& );

#endif
