#include "stack.h"

// Use this method for all your reallocations:

void stack::ensure_capacity( size_t c ) 
{
    if( current_capacity < c )
    {
      // New capacity will be the greater of c and
      // 2 * current_capacity. 

      if( c < 2 * current_capacity )
         c = 2 * current_capacity + 1;

      double* newdata = new double[ c ];
      for( size_t i = 0; i < current_size; ++ i )
         newdata[i] = data[i];

      current_capacity = c;
      delete[] data;
      data = newdata;
    }
}
stack::stack( ) 
	: current_size{ 0 }, 
	  current_capacity{ 17 }, 
	  data{ new double[current_capacity] }
   {}

stack::stack( const stack& s )
	: current_size{ s.current_size },
	  current_capacity{ s.current_capacity }, 
	  data{ new double[ current_capacity ] } 
   {
	
	for ( size_t i = 0; i < current_capacity; ++ i ) 
		data[i] = s. data[i];
    
    }

stack::stack( std::initializer_list<double> init ) 
    : current_size{ init. size() },
      current_capacity{ init.size() }, 
      data{ new double[ init.size() ] } 
{
    size_t i = 0;
    for ( double d: init ) 
    {
        data[i++] = d;
    }
}

void stack::push( double d ) 
{
	ensure_capacity( current_size + 1 );
	data[ current_size ] = d; 
	current_size++;
}

const stack& stack::operator = ( const stack& s )
{
	ensure_capacity( s. size() + 1);

	current_size = s.current_size;

    for( size_t i = 0; i != s. size( ) ; ++ i )
    	data[i] = s. data[i];
    

    return s;	
}

std::ostream& operator << ( std::ostream& out, const stack& st )
{
	out << "[";
    for( size_t i = 0; i < st. size( );  i++ )
    {
    	if( i > 0 )
    		out << ", ";

    	out << st. data[i];
    }
    out << "]";

    return out; 
}