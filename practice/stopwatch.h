#ifndef TIMER_INCLUDED
#define TIMER_INCLUDED 1

#include <iostream>
#include <chrono>

class stopwatch {
	
	std::chrono::high_resolution_clock::time_point start; 
	std::chrono::high_resolution_clock::time_point end; 
	
public:
	stopwatch( ) 
		: start{ std::chrono::high_resolution_clock::now( ) }
        { }
	
	void reset( ) {
		start = std::chrono::high_resolution_clock::now( ); 
	}
	
	double time( ) {
		end = std::chrono::high_resolution_clock::now( );
		return std::chrono::duration< double > ( end - start ).count( );
	}
};
#endif
