#include <iostream>
#include <vector>

void print( std::vector< char > vect ) {
        for(size_t i = 0; i != vect. size( ); i ++ )
                std::cout << vect. at(i);
        std::cout << "\n";
}

bool issubset( std::vector< char >& vect1, std::vector< char >& vect2 ) {
	bool is = false;
	#if 1
	for( size_t i = 0; i < 4; i++ ) {
		int k = 0;
		std::cout << i << "\n";
		for( size_t j = i; j < j + 3; j ++, k++ ) {
			std::cout << vect1[j] << " " << vect2[k] << "\n";
			if( vect1[j] == vect2[k] )
				return true;
		}
		if( is )
			return true;
	}
	#endif
	return true; 
}

int main( ) { 

	std::vector< char > vect1 = { 'a', 'b', 'c', 'd', 'e', 'f' };
	std::vector< char > vect2 = { 'b', 'c', 'd' };

	for( size_t i = 0; i != vect1. size( ); i ++ )
		std::cout << "i: " << i << " vect = " << vect1[i] << "\n";
	std::cout << vect1. size( ) << "\n";
	std::vector< char > vect;
	for( size_t i = 1; i < 4; i ++ )
		vect. push_back( vect1. at(i) );
	print( vect );
	if( vect == vect2 ) std::cout << "equal\n";
	if( issubset( vect1, vect2 ) ) 
		std::cout << "vect 2 is subset of vect1\n";

	return 0;
}
