/*
 * set.cpp
 *
 *  Created on: Mar 18, 2019
 *      Author: benja
 */

#include "set.h"

// From previous task:

bool equal( const std::string& s1, const std::string& s2 ) {
	if( s1. size( ) != s2. size( ) )
		return false;
	for( size_t i = 0; i != s1. size( ); i++ ) 
		if( tolower( s1[i] ) != tolower( s2[i] ) )
			return false;
	return true;
}

// Must be new written:
// It is important that equal and hash agree. That means:
//    If equal(s1,s2) then hash(s1) == hash(s2).

size_t hash( const std::string& st ) {

	size_t sum = 0;
	size_t length = st. size( ) - 1;
	
	for( size_t i = 0; i != st. size( ); i ++, length-- ) 
		sum+= ( size_t ) tolower( st[i] ) * pow( 31, length );
		
	return sum;

}

bool set::contains( const std::string& s ) const {

	size_t index = hash( s ) % buckets. size( );
	
	std::list< std::string > ls = buckets. at( index );

	for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {
		if( equal( *it, s ) )
			return true;
	}
	return false;
}

bool set::simp_insert( const std::string& s ) {
	size_t index = hash( s ) % getnrbuckets( );

	if( !contains( s ) ) {
		buckets. at(index). push_back( s );
		set_size++;
		return true;
	}	
	return false;
}

bool set::insert( const std::string& s ) {
	
	if( loadfactor( ) > max_load_factor ) 
		rehash( getnrbuckets( ) * 2 );
	return simp_insert( s ); 
}

void set::rehash( size_t newbucketsize ) {
	if( newbucketsize < 4 )
		newbucketsize = 4;
	
	std::vector< std::list< std::string >> newbuckets =
		std::vector< std::list< std::string >> ( newbucketsize );
	
	std::vector< std::list< std::string >> oldbuckets = buckets;
	
	buckets = newbuckets;
	
	std::list< std::string > ls;

	set_size = 0;
	for( size_t i = 0; i != oldbuckets. size( ); i ++ ) {
		ls = oldbuckets[i];
		for( auto it = ls. begin( ); it != ls. end( ); it ++ )
			simp_insert( *it );
	}

}

bool set::remove( const std::string& s ) {

	size_t index = hash( s ) % getnrbuckets( );
	
	std::list< std::string > ls = buckets[index];

	for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {
		if( equal( *it, s ) ) {
			ls. erase( it );
			set_size--;
			buckets[index] = ls;
			return true;
		}
	}
	return false;
}

void set::clear( ) {
	for( size_t i = 0; i != getnrbuckets( ); i ++ ) 
		buckets[i]. clear( );
	
	set_size = 0;
}

std::ostream& set::print( std::ostream& out ) const {
	std::list< std::string> ls;
	for( size_t i = 0; i != getnrbuckets( ); i ++ ) {
		out << "bucket[" << i << "]:\t{ ";
		ls = buckets[i];
		for( auto it = ls. begin( ); it != ls. end( ); it ++ ) {
			if( it != ls. begin( ) )
				out << ", ";
			out << *it;
		}
		out << " }\n"; 
	}
	return out;
}

std::ostream& set::printstatistics( std::ostream& out ) const
{
   std::cout << "set size =            " << size( ) << "\n";
   std::cout << "load factor =         " << loadfactor( ) << " ( max = " << max_load_factor << " )\n";
   std::cout << "standard deviation =  " << standarddev( ) << "\n";
   return out;
}


