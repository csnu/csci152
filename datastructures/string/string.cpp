
#include "string.h"

std::ostream& operator << ( std::ostream& out, const string& s )
{
   for( size_t i = 0; i != s. len; ++ i )
     out << s.p[i];

   return out; // so that printing can coninue. 
}


