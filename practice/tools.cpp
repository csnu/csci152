#include <iostream>
#include <fstream>
#include <string>

int howmanylines( std::string filename ) {
	
	int numberoflines;

	std::string line;
        std::fstream file( filename );

        if ( file. is_open( ) ) {
                while( getline( file, line ) ) {
                        std::cout << line << std::endl;
                        numberoflines++;
                }
                file. close( );
        }
	return numberoflines;
}
int main( ) {
	
	std::string filename;
	std::cout << "Enter a file name: ";
	std::cin >> filename;

	int numberoflines = howmanylines( filename );
	
	std::cout << filename << " has " << numberoflines << 
			" lines." << std::endl;

	return 0;
}
